// console.log("Hello World!");

// Array Methods
// For Array Methods, we have the mutator and non-mutator methods.

// Mutator Methods
/*
	- Mutator methods are function that "mutate" or change an array after they're created
	- These methods manipulate the original array. Performing tasks such as adding and removing elements
	- push(), pop(), unshift(), shift(), splice(), sort(), & reverse()
*/

console.log("%c Mutator Methods", 'font-weight: bold;');
let fruits =['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log(fruits);
console.log("");

// push()
/*
	-"Adds" in the "end" of the array AND returns the array's length [O, O, "X"];
	Syntax:
		arrayName.push()
*/

console.log('%c Push Method:', 'background: #89CFFB;');
let fruitslength = fruits.push('Mango');
console.log(fruitslength);
console.log("Mutated array from push method:")
console.log(fruits);

// Push multiple elements to an array
fruits.push('Avocado', 'Guava');
console.log("Mutated array from push method: (multiple values)");
console.log(fruits);
console.log("");

// pop()
/*
	"Removes" the last element in an array AND returns the removed element
	[O, O, "X"]
	Syntax:
		arrayName.pop();
*/
console.log('%c Pop Method:', 'background: #89CFFB');
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:");
console.log(fruits);
console.log("");

// unshift()
/*
	"Adds" at the start of an array
	["X", O, O]
	Syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('element A', 'elementB')
*/
console.log('%c Unshift Method:', 'background: #89CFFB');
fruits.unshift('Lime', 'Banana');
console.log("Mutated array from unshift method:");
console.log(fruits);
console.log("");

// shift()
/*
	Removes element at the "start" of an array AND returns the removed element
	Syntax:
		arrayName.shift();
*/
console.log('%c Shift Method:', 'background: #89CFFB');
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);
console.log("");

// splice()
/*
	Simultaneously removes element from a specified number and adds elements
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/
console.log('%c Splice Method:', 'background: #89CFFB');
fruits.splice(1, 2, 'Lime', 'Cherry');
console.log("Mutated array from splice method:");
console.log(fruits);
console.log("");

// sort()
/*
	Rearranges the array elements in an Alphanumeric order
	Syntax:
		arrayName.sort()
*/
console.log('%c Sort Method:', 'background: #89CFFB');
fruits.sort();
console.log("Mutated array from sort method:");
console.log(fruits);
console.log("");


// reverse()
/*
	Reverse the order of array elements
	Syntax:
		arrayName.reverse()
*/
console.log('%c Reverse Method:', 'background: #89CFFB');
fruits.reverse();
console.log("Mutated array from reverse method:");
console.log(fruits);
console.log("");

